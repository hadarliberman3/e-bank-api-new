import { RowDataPacket } from 'mysql2';
import { IIndividualAccount } from '../models/account.model.js';
import { IAccountDB, IAddressDB, IIndividualAccountDB } from '../models/account.db.model.js';

class IndividualConverter {
  rowDataPacketToModel(db_result: RowDataPacket[]): IIndividualAccount[] {
    const individualConverted: IIndividualAccount[] = [];
    for (let i = 0; i < db_result.length; i++) {
      const rowData = db_result[i] as any;
      const {
        account_id,
        currency,
        balance,
        status,
        type,
        first_name,
        last_name,
        email,
        country_name,
        country_code,
        postal_code,
        person_id,
        city,
        region,
        street_name,
        street_number,
      } = rowData;

      individualConverted.push({
        account_id,
        currency,
        balance,
        status,
        individual_id: person_id,
        first_name,
        last_name,
        email,
        type,
        address: {
          country_name,
          country_code,
          postal_code,
          city,
          region,
          street_name,
          street_number,
        },
      });
    }
    return individualConverted;
  }

  individualModelToDb(
    individual_account: IIndividualAccount,
  ): [IAccountDB, IAddressDB, IIndividualAccountDB] {
    const {
      currency,
      balance = 0,
      status,
      type = 'individual',
      agent_id = null,
      first_name,
      last_name,
      individual_id,
      address: {
        country_name,
        country_code,
        postal_code,
        city,
        region,
        street_name,
        street_number,
      },
      email,
    } = individual_account;

    const account_db: IAccountDB = {
      currency,
      balance,
      status,
      type,
      agent_id,
    };

    const address_db: IAddressDB = {
      country_name,
      country_code,
      postal_code,
      city,
      region,
      street_name,
      street_number,
    };

    //parse individual data
    const individual_db: IIndividualAccountDB = {
      person_id: individual_id,
      first_name,
      last_name,
      email,
      account_info_id: -1,
    };

    return [account_db, address_db, individual_db];
  }
}

const individual_converter = new IndividualConverter();

export default individual_converter;
