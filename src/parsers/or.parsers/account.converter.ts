import { RowDataPacket } from 'mysql2';
import { IAccountDB } from '../../models/account.db.model.js'
import { IAccount } from '../../models/account.model.js'

class AccountConverter {

  rowDataPacketToModel(db_result: RowDataPacket[]): IAccount[] {
    const accounts = db_result.map(rowData => {
      const { account_id, currency, balance, status, type } = rowData;
      const account: IAccount = {
        account_id,
        currency,
        balance,
        status,
        type,
      };

      return account;

    })


    return accounts;
  }



  accountModelToDb(account_model: IAccount): IAccountDB {
    const { account_id, currency, balance, status, type } = account_model;

    const account_db: IAccountDB = {
      account_id,
      currency,
      type,
      balance,
      status,
      agent_id: 1
    };

    return account_db;
  }
}

const account_converter = new AccountConverter();
export default account_converter;
