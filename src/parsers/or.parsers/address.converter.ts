import { RowDataPacket } from 'mysql2';
import { IAddress } from '../../models/account.model.js';

class AddressConverter {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  rowDataPacketToModel(db_result: RowDataPacket[]): IAddress[] {
    
   const addresses = db_result.map(rowData =>{

      const {
        address_id = -1,
        country_name,
        country_code,
        postal_code,
        city,
        region,
        street_name,
        street_number,
      } = rowData;
  
      const address: IAddress = {
        address_id,
        country_name,
        country_code,
        postal_code,
        city,
        region,
        street_name,
        street_number,
      };
      
      return address;

    } )
    
    return addresses;
  }

  
}

const address_converter = new AddressConverter();

export default address_converter;
