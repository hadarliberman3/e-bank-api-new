import { RowDataPacket } from 'mysql2';
import { IFamilyAccountDB } from '../../models/account.db.model.js';
import { IFamilyAccount, IIndividualAccount } from '../../models/account.model.js';
import { ICreateFamilyAccountDTO } from '../../models/requests.dto.model.js';
//import { IAccountDB, IFamilyAccountDB } from '../../models/account.db.model.js';
//import account_converter from './account.converter.js';
import AddressConverter from './address.converter.js';

// "context": "acto pashido",
// "family_account_id": 4,
// "first_name": "yonatan",
// "last_name": "maya",
// "address_id": 1,
// "individual_account_id": 1,
// "email": "yooni@gmail.com",
// "person_id": 10223554,
// "individual_currency": "uk",
// "individual_balance": 6700,
// "individual_status": "active",
// "individual_agent": 1,
// "family_currency": "uk",
// "family_balance": 0,
// "family_status": "active",
// "family_agent": 1,
// "country_name": "israel",
// "country_code": "12",
// "postal_code": 33,
// "city": "haifa",
// "region": "lo",
// "street_name": "roth",
// "street_number": 4,
// "created_at": "2022-02-13T08:28:48.000Z",
// "updated_at": "2022-02-13T08:28:48.000Z"

// eslint-disable-next-line @typescript-eslint/no-unused-vars

class FamilyConverter{


  familyDtoToModel(family_dto:ICreateFamilyAccountDTO) : IFamilyAccount{
    const family_model : IFamilyAccount = {
      context: family_dto.context as string,
      owners: family_dto.owner_tuples.map(tuple => tuple[0]) as unknown as IIndividualAccount[],
      currency: family_dto.currency,
      balance: family_dto.balance as number,
      status: 'active',
      type: 'family'
    }

    return family_model;
  }

  familyDtoToDbModel(family_dto:ICreateFamilyAccountDTO) : IFamilyAccountDB{
  
  
    const family_model = this.familyDtoToModel(family_dto);

    const family_db_model : IFamilyAccountDB = {
      context: family_model.context,
      account_info_id : family_model.account_id,
    }
  
    return family_db_model
     
  }


   




   rowDataPacketToModel(db_result: any[]) {
  
  
    const family : IFamilyAccount = db_result.reduce((family_acc : IFamilyAccount,rowData) =>{
      
      //address details
      const [address] = AddressConverter.rowDataPacketToModel([rowData] as RowDataPacket[]);
      
      //individual details
      const {individual_account_id,email,person_id,
        first_name,last_name,individual_currency,individual_balance,individual_status,individual_agent} = rowData
  
        
      //family detals
      const {context,family_account_id,family_currency,family_balance,family_status,family_agent} = rowData;
      
  
      const owner : IIndividualAccount =
      {
        account_id : individual_account_id,
        individual_id: person_id,
        first_name,
        last_name,
        address,
        email,
        currency: individual_currency,
        balance: individual_balance,
        status: individual_status,
        agent_id : individual_agent,
        type: 'Individual'
      } 
  
      family_acc.owners.push(owner)
      
      //family details
      family_acc.account_id = family_account_id;
      family_acc.context = context;
      family_acc.currency = family_currency;
      family_acc.balance = family_balance;
      family_acc.status = family_status;
      family_acc.agent_id = family_agent;
      
      return family_acc;
    },{
        owners: []
      } as unknown as IFamilyAccount)
  
    return family;
  }
  
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // export function familyModelToDb(family_account: IFamilyAccount): [IAccountDB, IFamilyAccountDB] {
    
  //   const { context } = family_account;
  
  //   //parse account data
  //   const account_db: IAccountDB  = account_converter.accountModelToDb(family_account);
  
  //   //parse family data
  //   const family_db: IFamilyAccountDB = {
  //     account_info_id: account_db.account_id || -1,
  //     context,
  //   };
  
  //   return [account_db, family_db];
  // }
  

}

const family_converter = new FamilyConverter();

export default family_converter;
