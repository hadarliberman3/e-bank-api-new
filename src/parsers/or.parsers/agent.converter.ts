import { RowDataPacket } from "mysql2";
import { IAgent } from "../../models/account.model.js";


class AgentConverter {
    rowDataPacketToModel(db_result: RowDataPacket[]): IAgent[] {
        const agents = db_result.map(rowData => {
            const { agent_id, access_key, secret_key } = rowData;
            const agent: IAgent = {
                agent_id, access_key, secret_key
            };
            return agent;
        })
        return agents;
    }
}

const agent_converter = new AgentConverter();

export default agent_converter;