/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { RowDataPacket } from 'mysql2';
import { IBusinessAccount } from '../../models/account.model.js';
import { IAccountDB, IAddressDB, IBusinessAccountDB } from '../../models/account.db.model.js';
import account_converter from './account.converter.js';
import address_converter from './address.converter.js';

class BusinessConverter {

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  rowDataPacketToModel(db_result: RowDataPacket[]): IBusinessAccount[] {
  const businesses =  db_result.map(rowData =>{

      const { company_id, company_name, context } = rowData;
      
      const [account] = account_converter.rowDataPacketToModel([rowData]);
      const [address] = address_converter.rowDataPacketToModel([rowData]);
      
      const business: IBusinessAccount = {
        ...account,
        address,
        company_id,
        company_name,
        context,
      };

       return business;
    })

    return businesses;
    
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  businessModelToDb(
    business_account: IBusinessAccount,
  ): [IAccountDB, IAddressDB, IBusinessAccountDB] {
    const { address, company_id, company_name, context } = business_account;

    //parse account data
    const account_db : IAccountDB = account_converter.accountModelToDb(business_account)
    
    //parse address data
    const address_db: IAddressDB = { ...address };

    //parse business data
    const business_db: IBusinessAccountDB = {
      company_id,
      company_name,
      context,
      account_info_id : account_db.account_id || -1,
      address_id: address_db.address_id || -1,
    };

    return [account_db, address_db, business_db];
  }
}

const business_converter = new BusinessConverter();
export default business_converter;
