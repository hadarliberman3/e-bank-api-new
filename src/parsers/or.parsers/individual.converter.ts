/* eslint-disable @typescript-eslint/no-unused-vars */
import { RowDataPacket } from 'mysql2';
import { IIndividualAccount } from '../../models/account.model.js';
import { IAccountDB, IAddressDB, IIndividualAccountDB } from '../../models/account.db.model.js';
import account_converter from './account.converter.js';
import address_converter from './address.converter.js';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

class IndividualConverter {
  
  rowDataPacketToModel(db_result: RowDataPacket[]): IIndividualAccount [] {
    const individuals = db_result.map(rowData =>{
      const { 
        account_info_id,
        person_id,
        first_name,
        last_name,
        email,
      } = rowData;
      
      const [account_info] = account_converter.rowDataPacketToModel(db_result)
      const [address] = address_converter.rowDataPacketToModel(db_result);
      

      const individual: IIndividualAccount = {
        // account_id: individual_id,
        //account_id: account_info_id,
        ...account_info,
        address,
        individual_id: person_id,
        first_name,
        last_name,
        email,
      };

      return individual;
    })

    return individuals;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  individualModelToDb(
    individual_account: IIndividualAccount,
  ): [IAccountDB, IAddressDB, IIndividualAccountDB] {
    const {
      individual_id,
      first_name,
      last_name,
      email,
      address,
    } = individual_account;

    //parse account data
    const account_db: IAccountDB = account_converter.accountModelToDb(individual_account);

    //parse address data
    const address_db: IAddressDB = { ...address};

    //parse individual data
    const individual_db: IIndividualAccountDB = {
      person_id: individual_id,
      first_name,
      last_name,
      email,
      address_id : address_db.address_id || -1,
      account_info_id: account_db.account_id || -1,
    };

    return [account_db, address_db, individual_db];
  }
}

const individual_converter = new IndividualConverter();

export default individual_converter;
