import express from 'express';
import BusinessController from '../controllers/business.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import {raw} from '../middlewares/common.middlewares.js';

const business_router = express.Router();

business_router.post('/', verifyAuth, raw(BusinessController.createBusiness));

business_router.get('/:id', verifyAuth,raw(BusinessController.getBusiness));

export default business_router;
