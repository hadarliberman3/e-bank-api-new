import express from 'express';
import FamilyController from '../controllers/family.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import {raw} from '../middlewares/common.middlewares.js';

const family_router = express.Router();

family_router.post('/', 
verifyAuth,
raw(FamilyController.createFamily));

family_router.get('/:id', verifyAuth,raw(FamilyController.getFamily));

family_router.delete('/:id',verifyAuth, raw(FamilyController.closeFamily));

family_router.delete('/add_members', verifyAuth, raw(FamilyController.addMembers));

family_router.delete('/remove_members', verifyAuth, raw(FamilyController.removeMembers));

// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
//family_router.get("/test",raw(family_controller.test))

export default family_router;
