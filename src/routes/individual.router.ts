import express from 'express';
import individualcontroller from '../controllers/individual.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import {raw} from '../middlewares/common.middlewares.js';
import { inputValidationMiddleware } from '../middlewares/validation.middlewares.js';
import { ValidationEntities, ValidationRoutes } from '../models/enums.js';

const IndividualController = express.Router();

const entityValidator = inputValidationMiddleware(ValidationEntities.INDIVIDUAL);

IndividualController.post(
  '/',
  verifyAuth,
  entityValidator(ValidationRoutes.CREATE),
  raw(individualcontroller.createIndividual),
);

IndividualController.get(
  '/:id',
  verifyAuth,
  entityValidator(ValidationRoutes.GET),
  raw(individualcontroller.getIndividual),
);

export default IndividualController;
