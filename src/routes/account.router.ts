import express from 'express';
import AccountController from '../controllers/account.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';

import {raw} from '../middlewares/common.middlewares.js';

const account_router = express.Router();

account_router.put('/status',verifyAuth, raw(AccountController.setStatus));

account_router.post('/b2b',verifyAuth, raw(AccountController.createTransferB2B));

account_router.post('/b2bfx', verifyAuth, raw(AccountController.createTransferB2B));

account_router.post('/b2i',verifyAuth, raw(AccountController.createTransferB2I));

account_router.post('/f2b',verifyAuth, raw(AccountController.createTransferF2B));

export default account_router;
