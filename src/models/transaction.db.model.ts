export interface ITransactionDB {
  transaction_id?: number;
  src_id: number;
  dest_id: number;
  transaction_type: string;
  amount: number;
}


export interface ITransferDB{
  transaction : ITransactionDB
  src_balance : number,
  dest_balance : number
}
