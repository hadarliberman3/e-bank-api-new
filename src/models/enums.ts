export enum ReponseFormat {
  SHORT = 'short',
  FULL = 'full',
}

export enum AccountAction {
  ACTIVATE = 'activate',
  DEACTIVATE = 'deactivate',
}

export enum ResponseStatus {
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

export enum AccountStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export enum ValidationEntities {
  INDIVIDUAL = 'individual',
  FAMILY = 'family',
  BUSINESS = 'business',
  ACCOUNT = 'account',
}

export enum ValidationRoutes {
  CHANGE_ACCOUNT_STATUS = 'active_deactive_account',
  TRANSFER = 'transfer',
  CREATE = 'create_account',
  GET = 'get_account',
  ADD = 'add_members',
  REMOVE = 'remove_members',
  CLOSE = 'close_family',
}
