import { AccountAction } from './enums';

export interface ICreateFamilyAccountDTO {
  currency: string;
  balance?: number;
  context?: string;
  owner_tuples: [owner_id: number, amount: number][];
}

export interface IAddRemoveFamilyMembersDTO {
  family_account_id: number;
  owner_tuples: [owner_id: number, amount: number][];
}

export interface ITransactionDTO {
  src: number;
  dest: number;
  amount: number;
}

export interface IChangeStatusDTO {
  action: AccountAction.ACTIVATE | AccountAction.DEACTIVATE;
  account_ids: number[];
}
