import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IAccountDB } from '../models/account.db.model.js';
import { IAccount } from '../models/account.model.js';
import AccountConverter from "../parsers/or.parsers/account.converter.js";


class AccountRepository {
  async createAccountDB(account: IAccountDB): Promise<number> {
    const sql = 'INSERT INTO account SET ?';
    try {
      const rows = await connection.query(sql, account);
      const inserted_account_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return inserted_account_id;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
  async getAccountsByIdDB(accounts_id: number[]): Promise<IAccount[]> {
    // const sql = `SELECT * FROM account WHERE account_id IN (${
    //   '?' + ',?'.repeat(accounts_id.length - 1)
    // })`;
    // try {
    //   const [rows] = await connection.query(sql, accounts_id);
    //   return AccountConverter.rowDataPacketToModel(rows as RowDataPacket[]);
      
    // } catch (err) {
    //   console.log(err);
    //   throw err;
    // }
    try {
      const selected_data:RowDataPacket[] = [];
      await connection.beginTransaction();
      for (const account_id of accounts_id) {
        const sql = `SELECT * FROM Account WHERE account_id = ?`;
        const selected_account = (await connection.query(sql,account_id))[0]
        selected_data.push((selected_account as any[])[0] as RowDataPacket)
      }
      await connection.commit();
      return AccountConverter.rowDataPacketToModel(selected_data);
    } catch (err) {
      console.log(err);
      await connection.rollback();
      throw err;
    }
  }
  

  //return array of ids and the first element is tha status
  async updateStatusesAccountsByIdDB(accounts_id: any[], status: string): Promise<RowDataPacket[]> {
    try {
      const sql = `UPDATE account SET status=? WHERE account_id IN (${
        '?' + ',?'.repeat(accounts_id.length - 1)
      })`;
      await connection.query(sql, [status, ...accounts_id]);
      accounts_id.push(status);
      return accounts_id as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async updateAccountAmountByIdDB(account_id: number, balance: number): Promise<RowDataPacket[]> {
    const sql = 'UPDATE account SET ? WHERE account_id=?';
    try {
      const [rows] = await connection.query(sql, [{ balance }, account_id]);
      //const rows = await this.getAccountsByIdDB([account_id]);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

}
export default new AccountRepository();
