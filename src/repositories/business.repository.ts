import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IBusinessAccountDB } from '../models/account.db.model.js';
import { IBusinessAccount } from '../models/account.model.js';
import BusinessConverter from "../parsers/or.parsers/business.converter.js";

class BusinessRepository {
  async createBusinessDB(business: IBusinessAccountDB): Promise<number> {
    const sql = 'INSERT INTO business_account SET ?';
    try {
      const rows = await connection.query(sql, business);
      const inserted_business_account_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return inserted_business_account_id; 
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getBusinessesByIdDB(businesses_id: number[]): Promise<IBusinessAccount[]> {
    const sql =
      'SELECT * FROM account as a ' +
      'INNER JOIN business_account as b ON b.account_info_id=a.account_id ' +
      'LEFT JOIN address as ad ON ad.address_id=b.address_id ' +
      `WHERE a.account_id IN (${'?' + ',?'.repeat(businesses_id.length - 1)})`;
    try {
      const [rows] = await connection.query(sql, businesses_id);
      return BusinessConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
export default new BusinessRepository();
