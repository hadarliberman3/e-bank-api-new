import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IAgent } from '../models/account.model.js';
import AgentConverter from '../parsers/or.parsers/agent.converter.js';

class AgentRepository {
    async getAgentByAccessKey(access_key: string): Promise<IAgent[]>{
        const sql =
            'SELECT * FROM agent ' +
            'WHERE access_key = ?';
        try {
            const [rows] = await connection.query(sql,access_key);
            return AgentConverter.rowDataPacketToModel(rows as RowDataPacket[]);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}


export default new AgentRepository();