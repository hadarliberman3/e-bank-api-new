/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import AccountRepository from '../repositories/account.repository.js';
import { IAccount } from '../models/account.model.js';

class AccountService {
  async setStatus(primary_ids: number[], status: string): Promise<IAccount[]> {
    await AccountRepository.updateStatusesAccountsByIdDB(primary_ids, status);
    return [];
  }


  async getAccountByID(account_ids: number[]){
    const accounts = await AccountRepository.getAccountsByIdDB(account_ids);
    return accounts;
  }

  
}

const account_service = new AccountService();
export default account_service;
