import { IAccount, IAgent, IBusinessAccount, IFamilyAccount } from '../models/account.model';

export class ValidationService {
  // Account

  public static validateActiveDeactiveAccount = (
    accounts: IAccount[],
    requested_accounts_num: number,
    action: string,
  ): boolean => {
    return (
      ValidationService.doEntitiesExist(accounts, requested_accounts_num) &&
      !ValidationService.checkAccountsTypes(accounts, 'family') &&
      ValidationService.isValidActiveDeactive(accounts, action)
    );
  };

  // Individual
  public static validateIndividualCreation = (account: IAccount[]): boolean => {
    return !ValidationService.doEntitiesExist(account, 1);
  };
  

  public static validateGetIndividual = (accounts: IAccount[]): boolean => {
    return ValidationService.doEntitiesExist(accounts, 1);
  };

  //Business
  public static validateGetBusiness = (accounts: IBusinessAccount[]): boolean => {
    return ValidationService.doEntitiesExist(accounts, 1);
  };
  

  // Family

  public static validateFamilyCreation = (
    accounts: IAccount[],
    accounts_tuples: [number, number][],
    expected_currency: string,
  ): boolean => {
    return (
      ValidationService.doEntitiesExist(accounts, accounts_tuples.length) &&
      ValidationService.checkAccountsStatuses(accounts, 'active') &&
      ValidationService.checkAccountsTypes(accounts, 'individual') &&
      ValidationService.checkAccountsCurrencies(accounts, expected_currency) &&
      ValidationService.checkIfIndividualsHaveEnoughMoney(
        accounts,
        accounts_tuples.map((tuple: [number, number]) => tuple[1]),
      )
    );
  };

  public static validateAddMembersToFamily = (
    family_account: IFamilyAccount,
    accounts_to_add: IAccount[],
    accounts_tuples: [number, number][],
  ): boolean => {
    return (
      ValidationService.doesAccountExist(family_account) &&
      ValidationService.checkAccountType(family_account, 'family') &&
      ValidationService.checkAccountStatus(family_account, 'active') &&
      ValidationService.doEntitiesExist(accounts_to_add, accounts_tuples.length) &&
      ValidationService.checkAccountsStatuses(accounts_to_add, 'active') &&
      ValidationService.checkAccountsTypes(accounts_to_add, 'individual') &&
      ValidationService.checkAccountsCurrencies(accounts_to_add, family_account.currency) &&
      ValidationService.checkIfIndividualsHaveEnoughMoney(
        accounts_to_add,
        accounts_tuples.map((tuple: [number, number]) => tuple[1]),
      )
    );
  };

  public static validateDeleteAccountsFromFamily = (
    family_account: IFamilyAccount,
    accounts_to_delete: IAccount[],
    accounts_tuples: [number, number][],
  ): boolean => {
    return (
      ValidationService.doesAccountExist(family_account) &&
      ValidationService.checkAccountType(family_account, 'family') &&
      ValidationService.checkAccountStatus(family_account, 'active') &&
      ValidationService.doEntitiesExist(accounts_to_delete, accounts_tuples.length) &&
      ValidationService.areAccountsInFamily(family_account.owners, accounts_to_delete) &&
      ValidationService.checkIfFamilyHasEnoughMoney(
        family_account.balance,
        accounts_tuples.map(tuple => tuple[1]),
      )
    );
  };

  public static validateCloseFamilyAccount = (family_account: IFamilyAccount): boolean => {
    return (
      ValidationService.doesAccountExist(family_account) &&
      ValidationService.checkAccountType(family_account, 'family') &&
      ValidationService.checkAccountStatus(family_account, 'active') &&
      family_account.owners.length === 0
    );
  };

  // Transactions
  public static validateTransferB2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src) &&
      ValidationService.doesAccountExist(dest) &&
      ValidationService.checkAccountStatus(src, 'active') &&
      ValidationService.checkAccountStatus(dest, 'active') &&
      ValidationService.checkAccountType(src, 'business') &&
      ValidationService.checkAccountType(dest, 'business') &&
      ValidationService.checkAccountCurrency(dest, src.currency) &&
      ValidationService.checkTransferLogic('B2B', src, dest, amount)
    );
  };

  public static validateTransferB2BFX = (
    src: IAccount,
    dest: IAccount,
    amount: number,
  ): boolean => {
    return (
      ValidationService.doesAccountExist(src) &&
      ValidationService.doesAccountExist(dest) &&
      ValidationService.checkAccountStatus(src, 'active') &&
      ValidationService.checkAccountStatus(dest, 'active') &&
      ValidationService.checkAccountType(src, 'business') &&
      ValidationService.checkAccountType(dest, 'business') &&
      !ValidationService.checkAccountCurrency(dest, src.currency) &&
      ValidationService.checkTransferLogic('B2B', src, dest, amount)
    );
  };

  public static validateTransferB2I = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src) &&
      ValidationService.doesAccountExist(dest) &&
      ValidationService.checkAccountStatus(src, 'active') &&
      ValidationService.checkAccountStatus(dest, 'active') &&
      ValidationService.checkAccountType(src, 'business') &&
      ValidationService.checkAccountType(dest, 'individual') &&
      ValidationService.checkAccountCurrency(dest, src.currency) &&
      ValidationService.checkTransferLogic('B2I', src, dest, amount)
    );
  };

  public static validateTransferF2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src) &&
      ValidationService.doesAccountExist(dest) &&
      ValidationService.checkAccountStatus(src, 'active') &&
      ValidationService.checkAccountStatus(dest, 'active') &&
      ValidationService.checkAccountType(src, 'family') &&
      ValidationService.checkAccountType(dest, 'business') &&
      ValidationService.checkAccountsStatuses((src as IFamilyAccount).owners, 'active') &&
      ValidationService.checkAccountsTypes((src as IFamilyAccount).owners, 'individual') &&
      ValidationService.checkAccountCurrency(dest, src.currency) &&
      ValidationService.checkAccountsCurrencies((src as IFamilyAccount).owners, src.currency) &&
      ValidationService.checkTransferLogic('F2B', src, dest, amount)
    );
  };

  // Agent
  
  public static validateGetAgent = (agents : IAgent[]): boolean => {
    return ValidationService.doEntitiesExist(agents, 1);
  };

  // utils
  private static checkTransferLogic = (
    transaction_type: string,
    src: IAccount,
    dest: IAccount,
    amount: number,
  ): boolean => {
    switch (transaction_type) {
      case 'B2B': {
        let allowed_amount_to_transfer =
          (src as IBusinessAccount).company_id === (dest as IBusinessAccount).company_id
            ? 10000
            : 1000;
        return src.balance - amount >= 10000 && amount <= allowed_amount_to_transfer;
      }
      case 'B2I': {
        return src.balance - amount >= 10000 && amount <= 1000;
      }
      case 'F2B': {
        return src.balance - amount >= 5000 && amount <= 5000;
      }
    }
    return true;
  };

  private static doesAccountExist = (account: IAccount): boolean => {
    return !!account;
  };

  private static doEntitiesExist = (
    accounts: IAccount[] | IAgent[],
    expected_accounts_num: number,
  ): boolean => {
    return accounts.length === expected_accounts_num;
  };

  private static checkAccountsStatuses = (
    accounts: IAccount[],
    expected_status: string,
  ): boolean => {
    return accounts.every(account =>
      ValidationService.checkAccountCurrency(account, expected_status),
    );
  };

  private static checkAccountStatus = (account: IAccount, expected_status: string): boolean => {
    return account.status === expected_status;
  };

  private static checkAccountsTypes = (accounts: IAccount[], expected_type: string): boolean => {
    return accounts.every(account => ValidationService.checkAccountType(account, expected_type));
  };

  private static checkAccountType = (account: IAccount, expected_type: string): boolean => {
    return account.type === expected_type;
  };

  private static checkAccountsCurrencies = (
    accounts: IAccount[],
    expected_currency: string,
  ): boolean => {
    return accounts.every(account =>
      ValidationService.checkAccountCurrency(account, expected_currency),
    );
  };

  private static checkAccountCurrency = (account: IAccount, expected_currency: string): boolean => {
    return account.currency === expected_currency;
  };

  private static checkIfIndividualsHaveEnoughMoney = (
    accounts: IAccount[],
    amounts_to_transfer: number[],
  ): boolean => {
    return accounts.every((account, index) => account.balance - amounts_to_transfer[index] > 1000);
  };

  private static checkIfFamilyHasEnoughMoney = (
    family_account_balance: number,
    amounts_to_transfer: number[],
  ): boolean => {
    for (const [index, amount] of amounts_to_transfer.entries()) {
      if (index === amounts_to_transfer.length - 1) {
        return family_account_balance - amount === 0 || family_account_balance - amount > 5000;
      } else {
        if (family_account_balance - amount < 0) {
          return false;
        }
        family_account_balance -= amount;
      }
    }
    return true;
  };

  private static areAccountsInFamily = (
    family_accounts: IAccount[],
    accounts: IAccount[],
  ): boolean => {
    const family_accounts_ids = family_accounts.map(account => account.account_id);
    const accounts_ids = accounts.map(account => account.account_id);
    return accounts_ids.every(account_id => family_accounts_ids.includes(account_id));
  };

  private static isValidActiveDeactive = (accounts: IAccount[], action: string): boolean => {
    if (action === 'activate') {
      return accounts.every(account => account.status === 'inactive');
    }
    return accounts.every(account => account.status === 'active');
  };
}
