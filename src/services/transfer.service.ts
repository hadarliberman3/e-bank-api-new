import account_service from './account.service.js';
import transaction_repository from '../repositories/transaction.repository.js';
import { ITransactionDB, ITransferDB } from '../models/transaction.db.model.js';
import { getRate } from '../utils/transfer.utils.js';
import { ITransactionDTO } from '../models/requests.dto.model.js';
import { IAccount } from '../models/account.model.js';
import AccountService from './account.service.js';
import { ValidationService } from './logic.validation.service.js';
import { HttpException } from '../exceptions/http.exception.js';


 class TransferService {
  
  private async prepareTransfer(
    src_account : IAccount,
    dest_account : IAccount,
    amount: number,
    transaction_type: string,
  ): Promise<ITransferDB> {
    
    let rate = 1;

    //check if currencies are different
    if (src_account.currency !== dest_account.currency) {
      //then get rate  
      rate = await getRate(src_account.currency, dest_account.currency);
    }

    //prepare tranfer db model
    const transaction :ITransactionDB = {
      src_id: src_account.account_id as number,
      dest_id: dest_account.account_id as number,
      transaction_type,
      amount,
    }
    const transfer_db_model: ITransferDB = {
      transaction,
      src_balance: src_account.balance - amount,
      dest_balance: dest_account.balance + amount * rate
    };

    return transfer_db_model;
  }

  async createTransactionB2B(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    const [src_account, dest_account] = await account_service.getAccountByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    const is_same_currency = src_account.currency === dest_account.currency;

    if(is_same_currency &&
      !ValidationService.validateTransferB2B(src_account,dest_account,transaction_dto.amount)){
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }
    else if(!is_same_currency &&
      !ValidationService.validateTransferB2BFX(src_account,dest_account,transaction_dto.amount)){
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }
    
    // prepare transfer for db
    const transfer_db_model : ITransferDB = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      'B2B',
    );
 
    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source,dest] = await AccountService.getAccountByID([transaction_dto.src,
      transaction_dto.dest]);

    return [source , dest];
  }


  async createTransactionB2I(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    const [src_account, dest_account] = await account_service.getAccountByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    
    if(!ValidationService.validateTransferB2I(src_account,dest_account,transaction_dto.amount)){
      console.log("validation service error")
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }
    
    // prepare transfer for db
    const transfer_db_model : ITransferDB = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      'B2I',
    );
 
    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source,dest] = await AccountService.getAccountByID([transaction_dto.src,
      transaction_dto.dest]);
      
    return [source , dest];
  }

  
  async createTransactionF2B(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    const [src_account, dest_account] = await account_service.getAccountByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    
    if(!ValidationService.validateTransferF2B(src_account,dest_account,transaction_dto.amount)){
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }

    // prepare transfer for db
    const transfer_db_model : ITransferDB = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      'F2B',
    );
 
    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source,dest] = await AccountService.getAccountByID([transaction_dto.src,
      transaction_dto.dest]);
      
    return [source , dest];
  }
 }

const transfer_service = new TransferService();
export default transfer_service;
