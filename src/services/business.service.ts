import { IBusinessAccount } from '../models/account.model.js';
import AccountRepository from '../repositories/account.repository.js';
import BusinessRepository from '../repositories/business.repository.js';
import AddressRepository from '../repositories/address.repository.js';
import business_converter from '../parsers/or.parsers/business.converter.js';
import { ValidationService } from './logic.validation.service.js';
import { IAccountDB, IAddressDB, IBusinessAccountDB } from '../models/account.db.model.js';
import { HttpException } from '../exceptions/http.exception.js';


class BusinessService {
  async create(business: IBusinessAccount): Promise<IBusinessAccount> {
    
      // Prepare models
      const db_models: [IAccountDB, IAddressDB, IBusinessAccountDB] =
        business_converter.businessModelToDb(business);
      const [account_db_model, address_db_model, business_db_model] = db_models;
  
      // Create address
      const inserted_address_id = await AddressRepository.createAdressDB(address_db_model);
  console.log(inserted_address_id);
      // Create account
      const inserted_account_id = await AccountRepository.createAccountDB(account_db_model);
  
      business_db_model.account_info_id = inserted_account_id;
      business_db_model.address_id = inserted_address_id;
  
      // Create business account
       await BusinessRepository.createBusinessDB(business_db_model);
  
      // CAN BE REPLACED WITH MERGING DBMODELS AND ADDING ACCOUNT ID AND ADDRESS ID
      const business_to_return = (
        await BusinessRepository.getBusinessesByIdDB([inserted_account_id])
      )[0];
  
      return business_to_return;
  }

  async getBusinessByID(business_id: number): Promise<IBusinessAccount> {
    const searched_accounts = await BusinessRepository.getBusinessesByIdDB([business_id]);
    if (!ValidationService.validateGetBusiness(searched_accounts)) {
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR 1');
    }
    const account_to_return = searched_accounts[0];
    return account_to_return;
  }
}
const business_service = new BusinessService();
export default business_service;
