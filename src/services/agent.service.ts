import { HttpException } from "../exceptions/http.exception.js";
import { IAgent } from "../models/account.model.js";
import AgentRepository from "../repositories/agent.repository.js";
import { ValidationService } from "./logic.validation.service.js";



class AgentService {
    async getAgentByAccessKey(access_key: string):Promise<IAgent> {
        const agents: IAgent[] = await AgentRepository.getAgentByAccessKey(access_key);
        if (!ValidationService.validateGetAgent(agents)) {
            throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
        }
        return agents[0];
    }   
}

export default new AgentService();