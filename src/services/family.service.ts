import { HttpException } from '../exceptions/http.exception.js';
import { IFamilyAccountDB, } from '../models/account.db.model.js';
import { IAccount,IFamilyAccount,IIndividualAccount } from '../models/account.model.js';
import { ReponseFormat } from '../models/enums.js';
import { ICreateFamilyAccountDTO } from '../models/requests.dto.model.js';
import FamilyRepository from '../repositories/family.repository.js';
import account_service from './account.service.js';
import { ValidationService } from './logic.validation.service.js';
//import AccountConverter from "../parsers/or.parsers/account.converter.js"


class FamilyService {
  async getByID(
    family_id: number,
    format: ReponseFormat.FULL | ReponseFormat.SHORT,
  ): Promise<IFamilyAccount | null> {
    const family = await FamilyRepository.getFamilyWithOwnersByIdDB(family_id) 

    if(format === ReponseFormat.SHORT){
        //make the answer short
        family.owners = family.owners.
        map(owner => owner.account_id) as unknown as IIndividualAccount[]
    }
    
    return family;
  }

  async closeFamily(
    family_id: number,
    format: ReponseFormat.FULL | ReponseFormat.SHORT,
  ): Promise<IFamilyAccount> {
    const test: IFamilyAccountDB = {
      family_id: 2,
      context: 'SSS',
      account_info_id: 23,
    };
    console.log(format);
    const response: any = await FamilyRepository.createFamilyDB(test);
    console.log(family_id);

    return response;
  }

  async create(family_DTO: ICreateFamilyAccountDTO, format: string): Promise<IFamilyAccount> {
   
    
     // Validate Family account creation
     //const email: string[] | null = individual.email ? [individual.email] : null;
    //  const searched_account = await IndividualRepository.getIndividualsByPersonIdOrEmailDB(
    //    [individual.individual_id],
    //    email,
    //  );

      const owner_accounts :  IAccount[] =  await account_service.getAccountByID(family_DTO.owner_tuples.
        map(tuple_owner => tuple_owner[0])
      )

     if (!ValidationService.validateFamilyCreation(owner_accounts,family_DTO.owner_tuples,family_DTO.currency)) {
       throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
     }

      
 
    // Prepare models
    //  const db_models: [IAccountDB] =
    //    AccountConverter.accountModelToDb(individual);
    //  const [account_db_model, family_db_model] = db_models;
 
    // const family_model : IFamilyAccount = {
    //   context: family_DTO.context as string,
    //   owners: [],
    //   currency: family_DTO.currency,
    //   balance: family_DTO.balance as number,
    //   status: 'active',
    //   type: 'family'
    // }
    //  const family_db_model : IFamilyAccountDB ={
    //    context: '',
    //    account_info_id = 
    //  }
 
    //  // Create account
    //  const inserted_account_id = await AccountRepository.createAccountDB(account_db_model);
 
    //  individual_db_model.account_info_id = inserted_account_id;
    //  individual_db_model.address_id = inserted_address_id;
 
    //  // Create individual account
    //  const inserted_individual_id = await IndividualRepository.createIndividualDB(
    //    individual_db_model,
    //  );
 
    //  // CAN BE REPLACED WITH MERGING DBMODELS AND ADDING ACCOUNT ID AND ADDRESS ID
    //  const individual_to_return = (
    //    await IndividualRepository.getIndividualsByIdDB([inserted_individual_id])
    //  )[0];
 
    //  return individual_to_return;

    console.log(format);
    const test: IFamilyAccountDB = {
      family_id: 2,
      context: 'SSS',
      account_info_id: 23,
    };
    const response: any = await FamilyRepository.createFamilyDB(test);
    console.log(response);

    return response;
  }

  async addMembers(
    family_id: number,
    owner_tuples: [number, number][],
    format: ReponseFormat.FULL | ReponseFormat.SHORT,
  ): Promise<IFamilyAccount> {
    const test: IFamilyAccountDB = {
      family_id: family_id,
      context: 'SSS',
      account_info_id: 23,
    };
    console.log(owner_tuples);
    console.log(format);
    const response: any = await FamilyRepository.createFamilyDB(test);
    return response;
  }

  async removeMembers(
    family_id: number,
    owner_tuples: [number, number][],
    format: ReponseFormat.FULL | ReponseFormat.SHORT,
  ): Promise<IFamilyAccount> {
    const test: IFamilyAccountDB = {
      family_id: family_id,
      context: 'SSS',
      account_info_id: 23,
    };
    console.log(owner_tuples);
    console.log(format);
    const response: any = await FamilyRepository.createFamilyDB(test);
    return response;
  }
}

const family_service = new FamilyService();

export default family_service;
