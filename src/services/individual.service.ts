import { IIndividualAccount } from '../models/account.model.js';
import AccountRepository from '../repositories/account.repository.js';
import IndividualRepository from '../repositories/individual.repository.js';
import AddressRepository from '../repositories/address.repository.js';
import individual_converter from '../parsers/individual.parser.js';
import { ValidationService } from './logic.validation.service.js';
import { IAccountDB, IAddressDB, IIndividualAccountDB } from '../models/account.db.model.js';
import { HttpException } from '../exceptions/http.exception.js';
class IndividualService {
  async create(individual: IIndividualAccount): Promise<IIndividualAccount> {
    // Validate Individual account creation
    const email: string[] | null = individual.email ? [individual.email] : null;
    const searched_account = await IndividualRepository.getIndividualsByPersonIdOrEmailDB(
      [individual.individual_id],
      email,
    );

    if (!ValidationService.validateIndividualCreation(searched_account)) {
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }

    // Prepare models
    const db_models: [IAccountDB, IAddressDB, IIndividualAccountDB] =
      individual_converter.individualModelToDb(individual);
    const [account_db_model, address_db_model, individual_db_model] = db_models;

    // Create address
    const inserted_address_id = await AddressRepository.createAdressDB(address_db_model);

    // Create account
    const inserted_account_id = await AccountRepository.createAccountDB(account_db_model);

    individual_db_model.account_info_id = inserted_account_id;
    individual_db_model.address_id = inserted_address_id;

    // Create individual account
     await IndividualRepository.createIndividualDB(individual_db_model );

    // CAN BE REPLACED WITH MERGING DBMODELS AND ADDING ACCOUNT ID AND ADDRESS ID
    const individual_to_return = (
      await IndividualRepository.getIndividualsByIdDB([inserted_account_id])
    )[0];

    return individual_to_return;
  }

  async getIndividualByID(individual_id: number): Promise<IIndividualAccount> {
    const searched_accounts = await IndividualRepository.getIndividualsByIdDB([individual_id]);
    if (!ValidationService.validateGetIndividual(searched_accounts)) {
      throw new HttpException(404, 'PLACEHOLDER FOR VALIDATION ERROR');
    }
    const account_to_return = searched_accounts[0];
    return account_to_return;
  }
}
const individual_service = new IndividualService();
export default individual_service;
