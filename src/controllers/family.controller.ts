import { RequestHandler } from 'express';
import {
  IAddRemoveFamilyMembersDTO,
  ICreateFamilyAccountDTO,
} from '../models/requests.dto.model.js';
import family_service from '../services/family.service.js';
import { ReponseFormat, ResponseStatus } from '../models/enums.js';
import { IFamilyAccount } from '../models/account.model.js';
import familyRepository from '../repositories/family.repository.js';
//import family_converter from '../parsers/or.parsers/family.converter.js';
class FamilyController {
  createFamily: RequestHandler = async (req, res) => {
    const family_to_create: ICreateFamilyAccountDTO = req.body;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family = await family_service.create(family_to_create, format);
    
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: family,
    });
  };

  getFamily: RequestHandler = async (req, res) => {
    // const family_id: number = Number(req.params.id);
    // const format =
    //   req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    // const family = await family_service.getByID(family_id, format);
    const family= await familyRepository.getFamilyWithOwnersByIdDB(4);
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: family,
    });
  };

  closeFamily: RequestHandler = async (req, res) => {
    const family_id = Number(req.params.id);
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family = await family_service.closeFamily(family_id, format);
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: family,
    });
  };

  removeMembers: RequestHandler = async (req, res) => {
    const remove_request: IAddRemoveFamilyMembersDTO = req.body;
    const { family_account_id, owner_tuples } = remove_request;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family: IFamilyAccount = await family_service.removeMembers(
      family_account_id,
      owner_tuples,
      format,
    );
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: family,
    });
  };

  addMembers: RequestHandler = async (req, res) => {
    const add_request: IAddRemoveFamilyMembersDTO = req.body;
    const { family_account_id, owner_tuples } = add_request;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family: IFamilyAccount = await family_service.addMembers(
      family_account_id,
      owner_tuples,
      format,
    );
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: family,
    });
  };

  test: RequestHandler = async (req, res) => {
    console.log("using repo ----------------------------------")
    const response_data0 = await familyRepository.getFamilyWithOwnersByIdDB(3)
    //const response_data1 = family_converter.rowDataPacketToModel(response_data0)
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data0,
    });
  };
}





export default new FamilyController();
