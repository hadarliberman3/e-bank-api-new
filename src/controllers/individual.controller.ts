import { RequestHandler } from 'express';
import { IIndividualAccount } from '../models/account.model.js';
import { ResponseStatus } from '../models/enums.js';
import individual_service from '../services/individual.service.js';

class IndividualController {
  createIndividual: RequestHandler = async (req, res) => {
    const individual_to_create: IIndividualAccount = req.body;
    const individual = await individual_service.create(individual_to_create);
    console.log("individual is")
    console.log(individual)
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: individual,
    });
  };

  getIndividual: RequestHandler = async (req, res) => {
    const individual_id: number = Number(req.params.id);
    const individual = await individual_service.getIndividualByID(individual_id);
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: individual,
    });
  };
}

export default new IndividualController();
