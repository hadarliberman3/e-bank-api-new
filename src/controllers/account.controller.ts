import { RequestHandler } from 'express';
import { IAccount } from '../models/account.model.js';
import { AccountAction, ResponseStatus } from '../models/enums.js';
import { IChangeStatusDTO, ITransactionDTO } from '../models/requests.dto.model.js';
import AccountService from '../services/account.service.js';
import TransferService from '../services/transfer.service.js';



class AccountController {
  setStatus: RequestHandler = async (req, res) => {
    const accounts_ids: number[] = (req.body as IChangeStatusDTO).account_ids;
    const action: AccountAction.ACTIVATE | AccountAction.DEACTIVATE = (req.body as IChangeStatusDTO)
      .action;
    const updated_accounts = await AccountService.setStatus(accounts_ids, action);
    const response_data = updated_accounts.map(account => {
      return {
        id: account.account_id,
        status: account.status,
      };
    });
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data,
    });
  };

  getAccounts: RequestHandler = async (req, res) => {
    //const accounts_ids: number[] = (req.body as IChangeStatusDTO).account_ids;
    //TEST
    const response_data = await AccountService.getAccountByID([2,3,1])
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data,
    });
  };

  createTransferB2B: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest: IAccount[] = await TransferService.createTransactionB2B(transaction_request);
    const response_data = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data,
    });
  };

  createTransferB2I: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest: IAccount[] = await TransferService.createTransactionB2I(transaction_request);
    const response_data = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data,
    });
  };

  createTransferF2B: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest: IAccount[] = await TransferService.createTransactionF2B(transaction_request);
    const response_data = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: response_data,
    });
  };
}

export default new AccountController();
