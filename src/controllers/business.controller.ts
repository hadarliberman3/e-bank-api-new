import { RequestHandler } from 'express';
import { IBusinessAccount } from '../models/account.model.js';
import { ResponseStatus } from '../models/enums.js';
import business_service from '../services/business.service.js';

class BusinessController {
  createBusiness: RequestHandler = async (req, res) => {
    const business_to_create: IBusinessAccount = req.body;
    const business = await business_service.create(business_to_create);
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: business,
    });
  };

  getBusiness: RequestHandler = async (req, res) => {
    const business_id = Number(req.params.id);
    const business = await business_service.getBusinessByID(business_id);
    res.status(200).json({
      status: ResponseStatus.SUCCESS,
      data: business
    });
  };
}

export default new BusinessController();
