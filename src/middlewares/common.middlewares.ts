import fs from 'fs';
import { RequestHandler } from 'express';

export const httpLogger = (path: string): RequestHandler => {
  const httpFile = fs.createWriteStream(path, { flags: 'a' });
  return (req, res, next): void => {
    httpFile.write(`${path} [${Date.now()}] ${req.method} ${req.path} \n`);
    next();
  };
};

export const raw = (func: RequestHandler): RequestHandler =>  {
  return async function (req, res, next) {
    try {
      await func(req, res, next);
    } catch (err) {
      next(err);
    }
  };
}
