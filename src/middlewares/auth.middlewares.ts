import { NextFunction, Request, RequestHandler, Response } from "express";
import AgentService from "../services/agent.service.js";
import crypto from 'crypto';
import { HttpException } from "../exceptions/http.exception.js";

export const verifyAuth: RequestHandler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    const { method: http_method } = req;
    const { 'access-key': access_key, salt, timestamp, signature } = req.headers as { 'access-key': string, salt: string, timestamp: string, signature: string};
    if(!access_key || !salt || !timestamp || !signature){
        next(new HttpException(404, "Place holder for auth Error"));
    }
    const agent = await AgentService.getAgentByAccessKey(access_key);
    const secret_key = agent.secret_key;
    
    if(!secret_key){
        next(new HttpException(404, "Place holder for auth Error"));
    }

    let body: string = '';
    if (JSON.stringify(req.body) !== '{}' && req.body !== '') {
        body = JSON.stringify(req.body);
    }
    const to_sign = (http_method) + (salt) + (timestamp) + (access_key) + (secret_key) + body;
    console.log(to_sign);
    let generated_signature = crypto.createHmac('sha256', secret_key).update(to_sign).digest("hex");
    console.log(generated_signature);
    console.log(signature);
    if (generated_signature === signature) {
        next();
    }else{
        next(new HttpException(404, "Place holder for auth Error signature") );
    }
    
};


