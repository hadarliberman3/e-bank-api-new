import { NextFunction, Request, RequestHandler, Response } from 'express';
import { validation_config_object } from '../config/validation.config.js';
import { ValidationRoutes } from '../models/enums.js';
import { IGeneralObject, IValidationItem } from '../modules/validation/validation.model.js';
import { InputValidationService } from '../modules/validation/validation.service.js';
import { curry } from '../utils/common.utils.js';

export const inputValidationMiddleware: (entity: string) => (route: string) => RequestHandler =
  curry((entity: string, route: string) => {
    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      const objectToSend = route === ValidationRoutes.CLOSE || route === ValidationRoutes.GET ? req.params : req.body;
      const validation_result = InputValidationService.getValidationResult(
        (validation_config_object[entity] as IGeneralObject)[route] as IValidationItem[],
        objectToSend as IGeneralObject,
      );
      validation_result.length ? res.status(500).send({ messages: validation_result }) : next();
    };
  });
