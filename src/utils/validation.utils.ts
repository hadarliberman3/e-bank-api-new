export const INPUT_VALIDATION_FUNCTIONS = {
  isNumberBiTuples: (input: any, val: boolean): boolean =>
    (Array.isArray(input) &&
      input.reduce((acc: boolean, tuple) => {
        return (
          acc &&
          Array.isArray(tuple) &&
          tuple.length === 2 &&
          !isNaN(Number(tuple[0])) &&
          !isNaN(Number(tuple[1]))
        );
      }, true)) === val,
  biTuplesSumGreaterThan: (input: any, val: number): boolean =>
    Array.isArray(input) &&
    input.reduce((acc: boolean, tuple) => {
      return (
        acc &&
        Array.isArray(tuple) &&
        tuple.length === 2 &&
        !isNaN(Number(tuple[0])) &&
        !isNaN(Number(tuple[1]))
      );
    }, true) &&
    input.reduce((acc: number, tuple: [number, number]) => acc + Number(tuple[1]), 0) > val,
  biTuplesAmountPositive: (input: any, val: boolean): boolean =>
    (Array.isArray(input) &&
      input.reduce((acc: boolean, tuple) => {
        return (
          acc &&
          Array.isArray(tuple) &&
          tuple.length === 2 &&
          !isNaN(Number(tuple[0])) &&
          !isNaN(Number(tuple[1])) &&
          Number(tuple[1]) >= 0
        );
      }, true)) === val,
  isExist: (input: any, val: boolean): boolean => !!input === val,
  isPositive: (input: any, val: boolean): boolean => Number(input) >= 0 === val,
  isNumber: (input: any, val: boolean): boolean => !isNaN(Number(input)) === val,
  greaterThan: (input: number, val: number): boolean => Number(input) >= val,
  numOfDigits: (input: number, val: number): boolean =>
    input !== undefined && !isNaN(Number(input)) && String(input).length === val,
  isEmptyArray: (input: any, val: boolean): boolean =>
    Array.isArray(input) && (input.length === 0) === val,
  isIn: (input: any, val: any[]): boolean => val.includes(input),
  isNumbersArray: (input: any, val: boolean): boolean =>
    Array.isArray(input) && input.every((ele: any) => !isNaN(Number(ele))) === val,
};

