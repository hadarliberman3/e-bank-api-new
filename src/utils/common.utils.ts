import { RequestHandler } from 'express';

export const curry = (
  f: (a: string, b: string) => RequestHandler,
): ((a: string) => (b: string) => RequestHandler) => {
  return (a: string): ((b: string) => RequestHandler) => {
    return (b: string): RequestHandler => {
      return f(a, b);
    };
  };
};

// export const  curry = <T1, T2, R>(f: (t1: T1, t2: T2) => R): CurriedFunction2<T1, T2, R> {
//   function curriedFunction(t1: T1): (t2: T2) => R;
//   function curriedFunction(t1: T1, t2: T2): R;
//   function curriedFunction(t1: T1, t2?: T2): any {
//       switch (arguments.length) {
//           case 1:
//               return function(t2: T2): R {
//                   return f(t1, t2);
//               }
//           case 2:
//               return f(t1, t2 as T2);
//       }
//   }
//   return curriedFunction;
// }

// interface CurriedFunction2<T1, T2, R> {
//   (t1: T1): (t2: T2) => R;
//   (t1: T1, t2: T2): R;
// }
