import {expect} from "chai";
import sinon from "sinon";
import { IBusinessAccount } from "../src/models/account.model.js";
import businessService from "../src/services/business.service.js";
import addressRepository from "../src/repositories/address.repository.js";
import businessRepository from "../src/repositories/business.repository.js";
import AccountRepository from "../src/repositories/account.repository.js";
import business_converter from "../src/parsers/business.parser.js";
import { IAccountDB, IAddressDB, IBusinessAccountDB } from "../src/models/account.db.model.js";
import {ADDRESS_ID,ACCOUNT_ID,mock_account_db, mock_business_db, mock_address_db,mock_business} from "./mock.data.js";
describe("Business service", function() {
    context("create function",()=>{
        it("should accept and return IBusinessAccount", async ()=> {
        //    before(()=>{
            sinon.stub(business_converter,"businessModelToDb").resolves([mock_account_db, mock_address_db, mock_business_db]);
            sinon.stub(addressRepository,"createAdressDB").resolves(ADDRESS_ID);
            sinon.stub(AccountRepository,"createAccountDB").resolves(ACCOUNT_ID);
            sinon.stub(businessRepository,"createBusinessDB").resolves(ACCOUNT_ID);
            sinon.stub(businessRepository,"getBusinessesByIdDB").resolves(mock_business);

         const result= await businessService.create(mock_business[0]);
 
          // eslint-disable-next-line @typescript-eslint/unbound-method
          expect(businessService.create).to.be.a("function");
        //@ts-ignore
           expect(result).to.eql(mock_business[0]);
        });
    })
    context("getBusinessByID function",()=>{
        it("should accept and return IBusinessAccount", async ()=> {
        //    before(()=>{
        //     sinon.stub(business_converter,"businessModelToDb").resolves([mock_account_db, mock_address_db, mock_business_db]);
        //     sinon.stub(addressRepository,"createAdressDB").resolves(1);
        //     sinon.stub(AccountRepository,"createAccountDB").resolves(7);
        //     sinon.stub(businessRepository,"createBusinessDB").resolves(7);
        //     sinon.stub(businessRepository,"getBusinessesByIdDB").resolves(mock_business);

        //  const result= await businessService.create(mock_business[0]);
 
        //   // eslint-disable-next-line @typescript-eslint/unbound-method
        //   expect(businessService.create).to.be.a("function");
        // //@ts-ignore
        //    expect(result).to.eql(mock_business[0]);
        });
    })
})
