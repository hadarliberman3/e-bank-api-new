import { IBusinessAccountDB,IAddressDB,IAccountDB,IFamilyAccountDB,IIndividualAccountDB } from "../src/models/account.db.model.js"
import { IBusinessAccount } from "../src/models/account.model.js"

export const ACCOUNT_ID=7;
export const ADDRESS_ID=1;

export const mock_address_db:IAddressDB={
    address_id: ADDRESS_ID,
    country_name: "Israel",
    country_code: "il",
    postal_code: 200,
    city: "Tel Aviv",
    region: "la",
    street_name: "hamasger",
    street_number: 22
   }
   
export const mock_business:IBusinessAccount[]=
[
{
    account_id: ACCOUNT_ID,
    currency: "EUR",
    balance: 1000,
    status: 'active',
    type: "business",
    agent_id: 1,
    company_id: 12345678,
    company_name: "rapyd",
    context: "transfers",
    address: mock_address_db
    }



]


export const mock_business_db:IBusinessAccountDB={
   
    business_id: 1,
    company_id: 12345678,
    company_name: "rapyd",
    context: "transfers",
    address_id: ADDRESS_ID,
    account_info_id: ACCOUNT_ID
}

export const mock_account_db:IAccountDB={
  account_id: ACCOUNT_ID,
  currency: "EUR",
  balance: 10000,
  status: 'active',
  type: 'business',
  agent_id:1
}



