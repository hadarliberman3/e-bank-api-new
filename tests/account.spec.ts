import {expect} from "chai";
import {hello} from "../src/api.js";

describe("set status function", function() {
     it("should exists", async ()=> {
        expect(hello).to.be.a("function");
        expect(hello).to.be.instanceOf(Function);
    });

      it("should return 1",()=> {
        
        const res= hello();
        expect(res).to.equal(2);
    
    });
//     it("should sum of many numbers", ()=> {
//         const mySum=sum(1,2,3,4);
//         expect(mySum).to.equal(10);
    
//     });
//     it("should exists", ()=> {
//         expect(multiply).to.be.a("function");
//         expect(multiply).to.be.instanceOf(Function);
//     });

//       it("should multiply the elements of the array with the multiplier", ()=> {
//         const mymultiply=multiply(2,[1,2]);
//         expect(mymultiply).to.eql([2,4]);
    
//     });
//     it("should multiply many numbers", ()=> {
//         const mymultiply=multiply(2,[2,3,4,3]);
//         expect(mymultiply).to.eql([4,6,8,6]);
    
//     });
  });